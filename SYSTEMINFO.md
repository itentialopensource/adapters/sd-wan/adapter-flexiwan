# flexiWAN

Vendor: flexiWAN
Homepage: https://flexiWAN.com/

Product: flexiWAN
Product Page: https://flexiWAN.com/

## Introduction
We classify flexiWAN into the SD-WAN/SASE domain as flexiWAN devices provide a Software Defined Wide Area Network (SD-WAN) solution.

"flexiWAN provides a modern approach for combining networking and security." 
"flexiWAN is better, simpler and faster SD-WAN solution." 

## Why Integrate
The flexiWAN adapter from Itential is used to integrate the Itential Automation Platform (IAP) with flexiWAN. With this adapter you have the ability to perform operations on items such as:

- Devices
- Routes
- Jobs
- Organizations

## Additional Product Documentation
The [API documents for flexiWAN](https://manage.flexiwan.com/api-docs/#/)
[flexiWAN Northbound API](https://docs.flexiwan.com/api/nbapi.html)
